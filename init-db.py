import sys
import csv
from datetime import date
from argparse import ArgumentParser
from configparser import ConfigParser
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from zope.sqlalchemy import register
import transaction
from common import one_day
from models import (
    Base,
    Bulan,
    Kalender,
    )
from tools import FromCSV


def date_from_str(s):
    day, month, year = [int(x) for x in s.split('-')]
    return date(year, month, day)


def hapus_hijriah_mulai_kini():
    kini = date.today()
    q = db_session.query(Kalender).filter_by(tgl=kini)
    row = q.first()
    if not row:
        return
    h_thn = row.h_thn
    if row.h_bln == 1:
        h_thn -= 1
    print(f'Hapus {h_thn} Hijriah ke atas')
    q = db_session.query(Kalender).filter(Kalender.h_thn >= h_thn)
    q.delete()


def kalender():
    tgl = conf['main']['awal_masehi']
    tgl = date_from_str(tgl) - one_day
    hapus_hijriah_mulai_kini()
    for h_thn_str in conf['hijriah']:
        h_thn = int(h_thn_str)
        q = db_session.query(Kalender).filter_by(h_thn=h_thn)
        q = q.order_by(Kalender.id.desc())
        found = q.first()
        if found:
            tgl = found.tgl
            continue
        print(f'Buat {h_thn} Hijriah')
        h_bln = 0
        jml_hari_setahun = [int(x) for x in conf['hijriah'][h_thn_str].split()]
        for jml_hari in jml_hari_setahun:
            h_bln += 1
            for h_tgl in range(1, jml_hari+1):
                tgl += one_day
                row = Kalender(tgl=tgl, h_tgl=h_tgl, h_bln=h_bln, h_thn=h_thn)
                row.save(db_session)


pars = ArgumentParser()
pars.add_argument('conf')
option = pars.parse_args(sys.argv[1:])

conf_file = option.conf
conf = ConfigParser()
conf.read(conf_file)

engine = create_engine(conf['main']['db_url'])

Base.metadata.create_all(engine)
session_factory = sessionmaker(bind=engine)
db_session = session_factory()
register(db_session)

with transaction.manager:
    c = FromCSV(Base, db_session)
    c.restore('bulan.csv', Bulan)
    kalender()
