from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (
    Column,
    Integer,
    String,
    Date,
    UniqueConstraint,
    ForeignKey,
    )


Base = declarative_base()


class CommonModel:
    def to_dict(self):
        d = dict()
        for column in self.__table__.columns:
            d[column.name] = getattr(self, column.name)
        return d

    def from_dict(self, values):
        for column in self.__table__.columns:
            if column.name in values:
                setattr(self, column.name, values[column.name])


class Bulan(Base, CommonModel):
    __tablename__ = 'hijriah_bulan'
    id = Column(Integer, primary_key=True)
    nama = Column(String(32), unique=True, nullable=False)


class Kalender(Base, CommonModel):
    __tablename__ = 'kalender'
    id = Column(Integer, primary_key=True)
    tgl = Column(Date, nullable=False, unique=True)
    h_tgl = Column(Integer, nullable=False)
    h_bln = Column(Integer, ForeignKey(Bulan.id), nullable=False)
    h_thn = Column(Integer, nullable=False)
    __table_args__ = (UniqueConstraint('h_tgl', 'h_bln', 'h_thn'),)

    def save(self, db_session):
        self.id = self.tgl.year * 10000 + self.tgl.month * 100 + self.tgl.day
        db_session.add(self)
