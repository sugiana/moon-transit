Mengetahui Puncak Purnama
=========================

Malam saat puncak purnama atau *full moon* berarti pagi harinya adalah tanggal
15 pada sistem bulan yang dikenal sebagai penanggalan Hijriah.


Pengamatan
----------

Berikut ini cara mengetahuinya tanpa bantuan komputer:

1. Siang sebelum pengamatan bulan dapatkan waktu saat matahari tepat di atas
   kepala yang disebut sebagai *sun transit*. Misalkan diperoleh jam 11:49.

2. Maka toleransi puncak purnama - yang disebut sebagai *moon transit* - pada malam
   harinya adalah::
   
    awal moon transit = sun transit + 12 jam = 23:49
    akhir moon transit = awal moon transit + 40 menit = 00:29

3. Di rentang waktu tersebut kita perlu **berdiri tepat menghadap utara atau
   selatan**, sesuai di arah mana bulan tampak.

4. Luruskan kedua tangan lalu angkat hingga bulan berada di antara keduanya.
   Jika sudah berada di tengah maka itulah waktu puncak purnama.
   
5. Catat tanggal dan jamnya. 


Perhitungan
-----------

Waktu *moon transit* dapat dihitung menggunakan script ``moon-transit.py``. Ia
membutuhkan file konfigurasi yang memuat:

1. Koordinat pengamatan
2. Zona waktu
3. Awal waktu penghitungan *sun transit*

Contohnya seperti pada file ``depok.ini``::

    [main]
    latitude = -6.4128
    longitude = 106.7882
    timezone = Etc/GMT-7

    [sun transit]
    start time = 11:30

Untuk koordinat Kota Magelang maka bisa dilihat di
`Google Maps <https://www.google.com/maps>`_. Pada kotak pencarian
ketik ``kota magelang`` nanti di tautan akan tampak seperti ini::

    https://www.google.com/maps/place/Magelang,+Kota+Magelang,+Jawa+Tengah/@-7.4729674,110.1825048,13z

Ini artinya::

    latitude = -7.4729674
    longitude = 110.1825048

Untuk wilayah Magelang nilai ``start time`` sebaiknya diisi ``11:15``.
Sebenarnya diisi jam ``11:00`` pun akan ketemu waktu *sun transit*, namun ini
berarti akan lebih lama prosesnya. Sehingga file ``magelang.ini`` secara
keseluruhan menjadi seperti ini::

    [main]
    latitude = -7.4729674
    longitude = 110.1825048
    timezone = Etc/GMT-7

    [sun transit] 
    start time = 11:15

Pastikan `Python 3.6 <https://s.id/python3>`_ ke atas sudah terpasang. Lalu buatlah Python Virtual
Environment::

    $ python3.6 -m venv ~/env
    $ ~/env/bin/pip install --upgrade pip wheel
    $ ~/env/bin/pip install -r requirements.txt

Lalu jalankan script tadi dengan file konfigurasi sebagai *input parameter*::

    $ ~/env/bin/python moon-transit.py depok.ini

Kali pertama dijalankan ia akan mencari file ``de421.bsp``. Bila belum ada maka
akan diunduh. Ini cukup lama, besarnya sekitar 17 MB. Berikut ini hasilnya::

    01-05-2022 11:50 sun transit
    Toleransi moon transit 23:50 - 00:30
    Pencarian 23:40 - 00:46
    Tidak terjadi moon transit di rentang itu.

Secara *default* tanggal yang digunakan adalah saat ini. Sekarang kita majukan
tanggalnya menjadi 16 Mei 2022 menggunakan *input parameter* ``--tgl``::

    $ ~/env/bin/python moon-transit.py depok.ini --tgl=16-5-2022

    16-05-2022 11:49 sun transit
    Toleransi moon transit 23:49 - 00:29
    Pencarian 23:39 - 00:45
    17-05-2022 00:19 moon transit

Seperti kita lihat waktu *moon transit* sudah berada di dalam rentang
toleransi yang berarti itulah puncak purnama. Kesimpulannya 17 Mei 2022
adalah tanggal 15 di tahun Hijriah.

Sekarang kita coba tanggal berikutnya yaitu 17 Mei 2022::

    $ ~/env/bin/python moon-transit.py depok.ini --tgl=17-5-2022

    17-05-2022 11:49 sun transit
    Toleransi moon transit 23:49 - 00:29
    Pencarian 23:39 - 00:45
    Tidak terjadi moon transit di rentang itu.

Tampak waktu *moon transit* sudah melebihi rentang toleransi yang berarti malam
18 Mei 2022 bukan puncak purnama. 


Hilal
-----

Hilal adalah sudut bulan terhadap horizon saat matahari terbenam. Secara
pengamatan dilakukan dengan cara:

1. Temukan daerah lapang hingga kita bia melihat ufuk timur. Ya, bulan juga
   terbit dari timur.
2. Gunakan teropong untuk melihatnya.
3. Ukur sudut ketinggiannya terhadap horizon. Entah bagaimana caranya :)

Alternatifnya dihitung oleh komputer::

    $ ~/env/bin/python hilal.py depok.ini --tgl=30-4-2022

    30-04-2022 17:44 ketinggian matahari 0.1 derajat dari horizon
    30-04-2022 17:44 ketinggian bulan -4.7 derajat dari horizon

Tanda minus menunjukkan bulan masih berada di bawah horizon, hilal belum
tampak. Lanjut hari berikutnya::

    $ ~/env/bin/python hilal.py depok.ini --tgl=1-5-2022

    01-05-2022 17:43 ketinggian matahari 0.2 derajat dari horizon
    01-05-2022 17:43 ketinggian bulan 5.4 derajat dari horizon

Tampak ketinggian bulan sudah positif, hilal sudah tampak. Apakah ini bulan
baru? Mungkin. Ada yang mensyaratkan minimalnya adalah 2 derajat.


Pembuatan Kalender
------------------
Salinlah file konfigurasinya::

  $ cp kalender.ini live.ini

Buat database lalu sesuaikan ``live.ini`` pada baris ``db_url``.
Kemudian buat tabelnya::

  $ ~/env/bin/python init-db.py live.ini

Sampai di sini tabel ``kalender`` sudah terisi dengan tanggal hasil pengamatan
*moon transit* dari tahun 1438 hingga 1444 Hijriah sesuai yang tertulis di file
konfigurasi section ``[hijriah]``.

Untuk membuat tahun berikutnya::

  $ ~/env/bin/python buat-kalender.py live.ini

Proses ini akan INSERT tahun 1445 Hijriah. Jadi setiap itu dijalankan akan
bertambah 1 tahun.

Semoga dipahami.
