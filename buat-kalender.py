import sys
from logging import getLogger
from datetime import (
    date,
    datetime,
    timedelta,
    )
from time import time
from argparse import ArgumentParser
from configparser import ConfigParser
import pytz
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from zope.sqlalchemy import register
import transaction
from transit import Transit
from common import one_day
from models import Kalender
from checksum import checksum
from tools import humanize_time
from logger import setup_logging


registry = dict()

hari27 = timedelta(27)
hari28 = timedelta(28)
hari15 = timedelta(15)
jam12 = timedelta(12/24)
menit40 = timedelta(40/24/60)


def best_moon(moon_list):
    # Cari yang paling dekat toleransi
    durasi_list = []
    for transit_time, default_start_time, default_end_time in moon_list:
        if transit_time < default_start_time:
            d = default_start_time - transit_time
            seconds = d.seconds
            level = 2
        elif transit_time > default_end_time:
            d = transit_time - default_end_time
            seconds = d.seconds
            level = 2
        else:
            seconds = 0
            level = 1
        durasi_list += [(level, seconds, transit_time)]
    for level, seconds, transit_time in durasi_list:
        print(level, seconds, transit_time)
    durasi_list.sort()
    return durasi_list[0][2]


def tf(t):
    return t.strftime('%d-%m-%Y %H:%M')


def __get_moon_transit(tgl_mulai, durasi_mundur=0, durasi_maju=0):
    log = getLogger('get_moon_transit')
    if option.mundur:
        tgl = tgl_mulai - hari28
    else:
        tgl = tgl_mulai + hari27
    n = 0
    moon_list = []
    while True:
        n += 1
        if n > 5:
            break
        if option.mundur:
            tgl -= one_day
        else:
            tgl += one_day
        # Sun
        start_time = datetime(tgl.year, tgl.month, tgl.day, hh, mm, tzinfo=tz)
        sun = Transit('sun', lat, lon, start_time)
        sun.show()
        # Moon
        default_start_time = sun.transit_time + jam12
        default_end_time = default_start_time + menit40
        log.debug(
            'Toleransi moon transit {} - {}'.format(
                tf(default_start_time), tf(default_end_time)))
        start_time = default_start_time - timedelta(durasi_mundur/24/60)
        durasi = durasi_mundur + 40 + durasi_maju
        end_time = start_time + timedelta(durasi/24/60)
        log.debug('Pencarian {} - {}'.format(tf(start_time), tf(end_time)))
        moon = Transit('moon', lat, lon, start_time, end_time)
        moon.show()
        if not moon.transit_time:
            continue
        log.debug('Moon transit {}'.format(tf(moon.transit_time)))
        moon_list += [[
            moon.transit_time, default_start_time, default_end_time]]
    return moon_list


def get_moon_transit(tgl_awal):
    moon_list = __get_moon_transit(tgl_awal)
    if moon_list:
        return moon_list[0][0]
    moon_list = __get_moon_transit(tgl_awal, 0, 18)
    moon_list += __get_moon_transit(tgl_awal, 10)
    if not moon_list:
        raise Exception('Tanggal 15 belum ditemukan')
    if not moon_list[1:]:
        return moon_list[0][0]
    return best_moon(moon_list)


def progress():
    tgl_str = tgl.strftime('%d-%m-%Y')
    h_str = f'{h_tgl}-{h_bln}-{h_thn}'
    if 'estimate' in registry:
        estimasi = ', estimasi ' + registry['estimate']
    else:
        estimasi = ''
    print(f'INSERT {tgl_str} = {h_str} Hijriah{estimasi}')


def get_h_15(tgl):
    waktu = get_moon_transit(tgl)
    if waktu.hour:  # Jam 23?
        return waktu.date() + one_day
    return waktu.date()


def get_h_awal():
    q = db_session.query(Kalender).filter_by(h_tgl=15)
    if option.mundur:
        q = q.order_by(Kalender.h_thn, Kalender.h_bln)
    else:
        q = q.order_by(Kalender.h_thn.desc(), Kalender.h_bln.desc())
    h_15_awal = q.first()
    tgl_str = h_15_awal.tgl.strftime('%d-%m-%Y')
    h_str = f'{h_15_awal.h_tgl}-{h_15_awal.h_bln}-{h_15_awal.h_thn}'
    print(f'Tanggal awal {tgl_str} = {h_str} Hijriah')
    q = db_session.query(Kalender).filter_by(
            h_tgl=1, h_bln=h_15_awal.h_bln, h_thn=h_15_awal.h_thn)
    h_1_awal = q.first()
    q = db_session.query(Kalender).filter_by(
            h_bln=h_15_awal.h_bln, h_thn=h_15_awal.h_thn)
    h_akhir_awal = q.order_by(Kalender.h_tgl.desc()).first()
    if option.mundur:
        if h_15_awal.h_bln == 1:
            h_bln = 12
            h_thn = h_15_awal.h_thn - 1
        else:
            h_bln = h_15_awal.h_bln - 1
            h_thn = h_15_awal.h_thn
    else:
        if h_15_awal.h_bln == 12:
            h_bln = 1
            h_thn = h_15_awal.h_thn + 1
        else:
            h_bln = h_15_awal.h_bln + 1
            h_thn = h_15_awal.h_thn
    return h_akhir_awal, h_15_awal, h_1_awal, h_bln, h_thn


def save_kalender():
    progress()
    if h_tgl < 1 or h_tgl > 30:
        raise Exception(
            'h_tgl tidak boleh kurang dari 1 dan tidak boleh lebih dari 30. '
            'Perbaiki script.')
    k = Kalender(tgl=tgl, h_tgl=h_tgl, h_bln=h_bln, h_thn=h_thn)
    k.save(db_session)


def get_estimate():
    durasi = time() - awal
    kecepatan = durasi / jml_tahun
    sisa_tahun = h_thn - jml_tahun
    estimasi = kecepatan * sisa_tahun
    return humanize_time(estimasi)


def recheck():
    checksum(db_session, h_thn)
    registry['estimate'] = get_estimate()


pars = ArgumentParser()
pars.add_argument('conf')
pars.add_argument('--mundur', action='store_true')
option = pars.parse_args(sys.argv[1:])

setup_logging(option.conf)

conf_file = option.conf
conf = ConfigParser()
conf.read(conf_file)

engine = create_engine(conf['main']['db_url'])

session_factory = sessionmaker(bind=engine)
db_session = session_factory()
register(db_session)

lat = conf.getfloat('moon_transit', 'latitude')
lon = conf.getfloat('moon_transit', 'longitude')

timezone = conf.get('moon_transit', 'timezone')
tz = pytz.timezone(timezone)

hh, mm = [int(x) for x in conf.get('moon_transit', 'start_time').split(':')]

jml_tahun = 0
awal = time()
while True:
    jml_tahun += 1
    h_akhir_awal, h_15_awal, h_1_awal, h_bln, h_thn = get_h_awal()
    with transaction.manager:
        h_15 = get_h_15(h_15_awal.tgl)
        h_tgl = 15
        tgl = h_15
        save_kalender()
        while True:
            if option.mundur:
                tgl += one_day
                if tgl == h_1_awal.tgl:
                    break
                h_tgl += 1
            else:
                h_tgl -= 1
                if h_tgl == 0:
                    break
                tgl -= one_day
            save_kalender()
        if option.mundur:
            tgl = h_15
            h_tgl = 15
            while True:
                h_tgl -= 1
                if h_tgl == 0:
                    break
                tgl -= one_day
                save_kalender()
        else:
            h_tgl_akhir = get_h_15(h_15)
            h_tgl_akhir -= hari15
            h_tgl = 15
            tgl = h_15
            while True:
                h_tgl += 1
                tgl += one_day
                save_kalender()
                if tgl == h_tgl_akhir:
                    break
        if option.mundur:
            if h_bln > 1:
                continue
        else:
            if h_bln < 12:
                continue
        recheck()
    if not option.mundur:
        break  # Kalau maju cukup 1 tahun saja
    if h_thn == 1:
        break
