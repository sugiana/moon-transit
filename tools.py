import csv
from datetime import (
    datetime,
    timedelta,
    )
from skyfield.api import (
    load,
    N,
    S,
    W,
    E,
    wgs84,
    )
from sqlalchemy import PrimaryKeyConstraint


planets = load('de421.bsp')
earth = planets['earth']
ts = load.timescale()

pilihan_objek = dict(
    sun='matahari',
    moon='bulan')


def observer_location(lat, lon):
    lat = lat < 0 and (-lat * S) or (lat * N)
    lon = lon < 0 and (-lon * W) or (lon * E)
    return earth + wgs84.latlon(lat, lon)


def degrees_from_horizon(observer, obj_name, this_time):
    obj = planets[obj_name]
    t = ts.from_datetime(this_time)
    from_position = observer.at(t)
    astrometric = from_position.observe(obj)
    app = astrometric.apparent()
    alt, az, distance = app.altaz()
    return alt.degrees


def datetime_from_str(s, tz):
    w = datetime.strptime(s, '%d-%m-%Y %H:%M')
    return datetime(w.year, w.month, w.day, w.hour, w.minute, tzinfo=tz)


def humanize_time(secs):
    mins, secs = divmod(secs, 60)
    hours, mins = divmod(mins, 60)
    return '%02d:%02d:%02d' % (hours, mins, secs)


def get_pkeys(table):
    r = []
    for c in table.constraints:
        if c.__class__ is PrimaryKeyConstraint:
            for col in c:
                r.append(col.name)
            return r
    return r


class FromCSV:
    def __init__(self, Base, db_session):
        self.Base = Base
        self.db_session = db_session

    def restore(self, filename, table):
        keys = get_pkeys(table.__table__)
        f = open(filename)
        reader = csv.DictReader(f)
        for source in reader:
            filter_ = {}
            for key in keys:
                if key in source:
                    filter_[key] = source[key]
                else:
                    uniq = reader.fieldnames[0]
                    filter_[uniq] = source[uniq]
            q = self.db_session.query(table).filter_by(**filter_)
            if q.first():
                continue
            row = table()
            row.from_dict(source)
            self.db_session.add(row)
            self.db_session.flush()
        f.close()
