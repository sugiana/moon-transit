from logging import getLogger
from tools import (
    observer_location,
    degrees_from_horizon,
    )
from common import one_minute


class FromHorizon:
    def __init__(self, lat, lon, name='sun'):
        self.name = name
        self.observer = observer_location(lat, lon)
        self.log = getLogger(__name__)
        self.log.debug(f'Pengamat {self.observer}')

    def search(self, start_time, degrees):
        last_alt = None
        old_sign = None
        local_time = start_time
        while True:
            current_degrees = degrees_from_horizon(
                    self.observer, self.name, local_time)
            if current_degrees > degrees:
                new_sign = '>'
            else:
                new_sign = '<'
            waktu = local_time.strftime('%d-%m-%Y %H:%M')
            self.log.debug(
                f'{waktu} elevasi {current_degrees:.2f} {new_sign} {degrees}')
            if old_sign and old_sign != new_sign:
                break
            last_degrees = current_degrees
            last_local_time = local_time
            old_sign = new_sign
            local_time += one_minute
        return local_time, last_degrees
