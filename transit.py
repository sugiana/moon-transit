from logging import getLogger
from datetime import timedelta
from tools import (
    degrees_from_horizon,
    observer_location,
    )
from common import one_minute


max_duration = timedelta(1)


class Transit:
    def __init__(self, name, lat, lon, start_time, end_time=None):
        self.name = name
        self.observer = observer_location(lat, lon)
        self.start_time = start_time
        self.end_time = end_time
        self.transit_time = None

    def search(self):
        last_time = None
        sudah_naik = False
        current_time = self.start_time
        log = getLogger('transit_search')
        while True:
            current_degrees = degrees_from_horizon(
                self.observer, self.name, current_time)
            t = current_time.strftime('%H:%M')
            log.debug(f'{t}: {current_degrees:.2f} derajat')
            if last_time:
                if sudah_naik:
                    if current_degrees > 0 and current_degrees < last_degrees:
                        break
                elif current_degrees > 0 and current_degrees > last_degrees:
                    sudah_naik = True
            if self.end_time and current_time >= self.end_time:
                return
            last_degrees = current_degrees
            last_time = current_time
            current_time += one_minute
            if current_time - self.start_time >= max_duration:
                raise Exception('Pencarian terlalu lama, perbaiki script.')
        self.transit_time = last_time

    def show(self):
        self.search()
        if not self.transit_time:
            return
        log = getLogger('transit_show')
        name = self.name.title()
        t = self.transit_time.strftime('%d-%m-%Y %H:%M')
        log.info(f'{name} transit {t}')
