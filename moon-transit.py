# Menentukan tanggal 15 Hijriah
import sys
from datetime import (
    date,
    datetime,
    timedelta,
    )
from argparse import ArgumentParser
from configparser import ConfigParser
import pytz
from transit import Transit
from logger import setup_logging


tgl = date.today().strftime('%d-%m-%Y')
help_tgl = f'default {tgl}'

durasi_mundur = 10
help_durasi_mundur = f'default {durasi_mundur}'

durasi_maju = 16
help_durasi_maju = f'default {durasi_maju}'

parser = ArgumentParser()
parser.add_argument('conf')
parser.add_argument('--tgl', default=tgl, help=help_tgl)
parser.add_argument(
        '--durasi-mundur', type=int, default=durasi_mundur,
        help=help_durasi_mundur)
parser.add_argument(
        '--durasi-maju', type=int, default=durasi_maju, help=help_durasi_maju)
option = parser.parse_args(sys.argv[1:])

setup_logging(option.conf)

conf = ConfigParser()
conf.read(option.conf)

timezone = conf.get('main', 'timezone')
tz = pytz.timezone(timezone)
tgl = datetime.strptime(option.tgl, '%d-%m-%Y')
hh, mm = [int(x) for x in conf.get('sun transit', 'start time').split(':')]

durasi_mundur = option.durasi_mundur
durasi_maju = option.durasi_maju

# Sun
start_time = datetime(tgl.year, tgl.month, tgl.day, hh, mm, tzinfo=tz)
lat = conf.getfloat('main', 'latitude')
lon = conf.getfloat('main', 'longitude')
sun = Transit('sun', lat, lon, start_time)
sun.search()
t = sun.transit_time.strftime('%d-%m-%Y %H:%M')
print(f'{t} sun transit')

# Moon
start_time = sun.transit_time + timedelta(12/24)
end_time = start_time + timedelta(40/24/60)
print(
    'Toleransi moon transit {} - {}'.format(
        start_time.strftime('%H:%M'),
        end_time.strftime('%H:%M')))
start_time -= timedelta(durasi_mundur/24/60)
durasi = durasi_mundur + 40 + durasi_maju
end_time = start_time + timedelta(durasi/24/60)
print(
    'Pencarian {} - {}'.format(
        start_time.strftime('%H:%M'),
        end_time.strftime('%H:%M')))
moon = Transit('moon', lat, lon, start_time, end_time)
moon.search()
if moon.transit_time:
    t = moon.transit_time.strftime('%d-%m-%Y %H:%M')
    print(f'{t} moon transit')
else:
    print('Tidak terjadi moon transit di rentang itu.')
