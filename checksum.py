from logging import getLogger
from sqlalchemy import func
from common import one_day
from models import Kalender


def checksum(db_session, h_thn):
    log = getLogger('checksum')
    q = db_session.query(Kalender.h_bln, func.count().label('count')).filter(
            Kalender.h_thn == h_thn).group_by(Kalender.h_bln)
    jml_hari = 0
    for row in q.order_by(Kalender.h_bln.desc()):
        log.debug(f'Jumlah hari bulan {row.h_bln} = {row.count}')
        jml_hari += row.count
        if row.h_bln == 9:
            if row.count not in (28, 29, 30):
                raise Exception(
                        f'Jumlah hari bulan {row.h_bln} salah '
                        'seharusnya 28, 29, atau 30')
        elif row.count not in (29, 30):
            raise Exception(
                    f'Jumlah hari bulan {row.h_bln} salah '
                    'seharusnya 29 atau 30')
    if jml_hari in (354, 355):
        log.debug(f'Tahun {h_thn} {jml_hari} hari')
    else:
        raise Exception(
                f'Tahun {h_thn } {jml_hari} hari seharusnya 354 atau 355')
