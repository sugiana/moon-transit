import sys
from datetime import date
from argparse import ArgumentParser
from configparser import ConfigParser
import pytz
from tools import (
    datetime_from_str,
    degrees_from_horizon,
    )
from from_horizon import FromHorizon
from logger import setup_logging


tgl = date.today().strftime('%d-%m-%Y')
help_tgl = f'default {tgl}'

parser = ArgumentParser()
parser.add_argument('conf')
parser.add_argument('--tgl', default=tgl, help=help_tgl)
option = parser.parse_args(sys.argv[1:])

setup_logging(option.conf)

conf = ConfigParser()
conf.read(option.conf)

timezone = conf.get('main', 'timezone')
tz = pytz.timezone(timezone)
start_time = datetime_from_str(f'{option.tgl} 17:00', tz)

lat = conf.getfloat('main', 'latitude')
lon = conf.getfloat('main', 'longitude')
sun = FromHorizon(lat, lon, 'sun')

waktu, sudut = sun.search(start_time, 0)
waktu_s = waktu.strftime('%d-%m-%Y %H:%M')
print(f'{waktu_s} ketinggian matahari {sudut:.1f} derajat dari horizon')

sudut = degrees_from_horizon(sun.observer, 'moon', waktu)
print(f'{waktu_s} ketinggian bulan {sudut:.1f} derajat dari horizon')
